## comparison of `bzip2` and `lbzip2` wall-time performance on a wikidata dump

### test data
```
$ ls -lh *bz2
lrwxrwxrwx 1 sean sean  69 Apr  4 15:29 test.bz2 -> wikidatawiki-20180301-pages-meta-current27.xml-p37586178p39086178.bz2
-rw-rw-r-- 1 sean sean 37G Mar 28 21:21 wikidatawiki-20180301-pages-meta-current27.xml-p37586178p39086178.bz2
```

### environment
* 2x 8-core Intel Xeon E5-2670 2.6GHz
* Centos 7
* 128GB
* SSD
* gcc 5.2, -g -O2

### results

| run | bzip2 | lbzip2 N=1 | lbzip2 N=2 | lbzip2 N=4 | lbzip2 N=6 | lbzip2 N=8 |
| --- | ---: | ---: | ---: | ---: | ---: | ---: |
| decompress | 32m36.700s | 22m55.798s | 11m52.971s | 6m6.895s | 4m15.801s | 3m34.393s |
| decompress, tail, head | 36m20.663s | 22m44.202s | 12m38.685s | 6m25.288s | 4m25.309s | 3m30.661s |
| decompress, recompress | 304m10.614s | 72m23.436s | 36m3.857s | 18m29.597s | 12m50.524s | 10m1.217s |

(wall-time, slowest runs after cache warm-up)

#### decompress

```bash
lbzcat -n $N test.bz2 >/dev/null
```

#### decompress, tail, head

```bash
lbzcat -n $N test.bz2 | tail -n +40 | head -n -1 >/dev/null
```

#### decompress, recompress

```bash
lbzcat -n $N test.bz2 | lbzip2 -n $N >/dev/null
```

### notes
* No significant %iowait at any point
* For the recompression run the CPU usage split was:
    * bzip
        * 10% decompress, 90% compress
        * 99% user / <1% system
    * lbzip2
        * 40% decompress / 60% recompress 
        * 85% user / 15% system