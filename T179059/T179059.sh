#!/bin/bash

dec=$1
com=$2
data=$3

echo "$dec $data >/dev/null"
time {
	$dec $data >/dev/null
}
echo

echo "$dec $data | tail -n +40 | head -n -1 >/dev/null"
time {
	$dec $data | tail -n +40 | head -n -1 >/dev/null
}
echo

echo "$dec $data | $com >/dev/null"
time {
	$dec $data | $com >/dev/null
}
echo
